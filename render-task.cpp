#include <iostream>
#include <string>
#include <ev++.h>
#include "render-task.h"

void
RenderTask::run()
{
    std::cout << "Running task." << std::endl;
    this->setOutput(new std::string(this->getData()));
    this->setStatus(READY);
}
