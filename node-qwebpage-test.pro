#-------------------------------------------------
#
# Project created by QtCreator 2011-07-07T00:02:01
#
#-------------------------------------------------

QT       += webkit

TARGET = node-qwebpage-test
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    render-task.cpp

#SOURCES += main.cpp \
    #handler.cpp

#HEADERS += ../node-qwebpage/qsrc/nodeqwebpage.h \
#    handler.h

#-------------------------------------------------
#
# There is a difference between static linking and
# dynamic linking. With static linking the full path to
# library must be specified. But with dynamic linking
# we need only pass the name of the library as in the
# commented line below.
# With static linking the library needs to exist only
# while building. But with dynamic linking, the name of
# the library gets recorded and that name will be
# searched for at runtime.
#
#LIBS += -L../node-qwebpage/build -lnode-qwebpage
#
#-------------------------------------------------
LIBS += /Users/rajiv/.builds/lib/libboost_thread.a
LIBS += /Users/rajiv/.builds/lib/libev.a

INCLUDEPATH += /Users/rajiv/.builds/include

HEADERS += \
    thread-object.h \
    task.h \
    render-task.h \
    task-queue.h \
    serial-task-runner.h
