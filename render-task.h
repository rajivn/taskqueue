#ifndef RENDERTASK_H
#define RENDERTASK_H

#include <string>
#include <ev++.h>

#include "task.h"

//
// A special type of task that will take a string that indicates
// a URL and tries to render it.
//
class RenderTask : public Task<std::string, std::string>
{
public:
    RenderTask(std::string *data)
        : Task<std::string, std::string>(data)
    {
    }

    RenderTask(const RenderTask &task)
        : Task<std::string, std::string>(task)
    {
    }

    virtual void run();
};

#endif // RENDERTASK_H
