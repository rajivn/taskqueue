#ifndef TASK_H
#define TASK_H

#include <iostream>
#include <ev++.h>
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>

//
// Abstract class for creation of tasks that perform operations.
//

//===================
// CLASS DECLARATION
//===================

// 'O' for Output
template<typename T, typename O = int>
class Task
{
public:
    enum Status {
        BUSY,
        ERROR,
        READY
    };

private:
    boost::shared_ptr<T> data;
    boost::shared_ptr<O> output;
    Status status;
    boost::mutex mutex;

public:
    // Default constructor
    /**
     * Pass in a data object which will be used to fuel this task.
     * After the task completes, the memory reserved for data will be
     * freed.
     */
    Task(T *data = NULL)
    {
        if (data != NULL) {
            this->data.reset(data);
        }
        this->status = BUSY;
        std::cout << "Task created." << std::endl;
    }
    // Copy constructor
    Task(const Task &task)
    {
        this->data = task.data;
        this->output = task.output;
        this->status = task.status;
        std::cout << "Task copied." << std::endl;
    }
    // Destructor
    virtual ~Task()
    {
        std::cout << "Task destroyed." << std::endl;
    }

    // getter for data
    T getData() const
    {
        T *data = this->data.get();
        return data ? *data : NULL;
    }

    // getter for status
    int getStatus() const
    {
        return status;
    }

    O *result() const
    {
        return this->output.get();
    }

    //
    // These are methods that should be implemented by children.
    //
    virtual void run() = 0;

protected:
    void setOutput(O *out)
    {
        this->output.reset(out);
    }

    void setStatus(Status status)
    {
        this->status = status;
    }
};

#endif // TASK_H
