#ifndef HANDLER_H
#define HANDLER_H

#include <QObject>

class Handler : public QObject
{
    Q_OBJECT

public:
    Handler();
    ~Handler();

public slots:
    void handle(void);
};


#endif // HANDLER_H
