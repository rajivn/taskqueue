#include <iostream>
#include <string>

#include "render-task.h"
#include "task-queue.h"

#define WARN_INVALID_TASK "WARNING: task incomplete"

int
main()
{
    // Create the task queue and add some tasks
    RenderTaskQueue queue;
    RenderTask task1(new std::string("No Dice!"));
    RenderTask task2(new std::string("Hello World!"));
    queue.add(&task1);
    queue.add(&task2);

    queue.start();

    RenderTask task3(new std::string("Tick Tock!"));
    RenderTask task4(new std::string("Hola Mundo!"));
    queue.add(&task3);
    queue.add(&task4);

    queue.start();

    // When we are out of the event loop we can make sure that the
    // task was performed.
    std::cout << (task1.getStatus() == task1.READY ? *(task1.result()) : WARN_INVALID_TASK) << std::endl;
    std::cout << (task2.getStatus() == task2.READY ? *(task2.result()) : WARN_INVALID_TASK) << std::endl;
    std::cout << (task3.getStatus() == task3.READY ? *(task3.result()) : WARN_INVALID_TASK) << std::endl;
    std::cout << (task4.getStatus() == task4.READY ? *(task4.result()) : WARN_INVALID_TASK) << std::endl;

    return 0;
}
