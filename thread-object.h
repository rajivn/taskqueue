#ifndef THREADOBJECT_H
#define THREADOBJECT_H

#include <ev++.h>

#include "task.h"
#include "task-queue.h"

//
// Responsible for fetching jobs from the parent and running them.
// When there are no more jobs remaining to process, it should start a
// timer loop to periodically look for tasks.
// It should also be capable of stopping the loop and breaking out when there
// are no more jobs to process.
//
//template< typename S, typename O, class E = Task<S, O>, class T = TaskQueue<S, E> >
//class ThreadObject
//{
//public:
//    ThreadObject(const T *queue)
//    {
//        this->queue = queue;
//        m_waiting = false;
//        m_should_quit = false;
//    }

//    ~ThreadObject()
//    {
//    }

//    // This will start the event loop.
//    void start()
//    {
//        if (this->queue != NULL) {
//            curTask = this->queue->next();
//            if (curTask != NULL) {
//                curTask->run();
//            }
//        }
//    }

//    // This is responsible for stopping the event loop.
//    void stop(ev::io &watcher);

//private:
//    const T *queue;
//    E *curTask;
//    bool m_waiting;
//    bool m_should_quit;
//};

#endif // THREADOBJECT_H
