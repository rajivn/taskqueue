#ifndef TASKQUEUE_H
#define TASKQUEUE_H

#include <signal.h>
#include <iostream>
#include <queue>
#include <list>
#include <boost/thread.hpp>

#include "task.h"
#include "render-task.h"
#include "serial-task-runner.h"

using namespace std;

template< typename T, class E = Task<T> >
class TaskQueue
{
private:
    class TaskRunner
    {
    private:
        int num;
        boost::lock_guard<boost::mutex> *guard;
        TaskQueue<T, E> *parent;

    public:
        TaskRunner(TaskQueue<T, E> *p, int n)
        {
            parent = p;
            num = n;
            cout << "Created runner " << num << endl;
        }

        ~TaskRunner()
        {
            cout << "Deleting " << num << endl;
        }

        void process(boost::mutex *mutex)
        {
            E *task = NULL;
            // lock
            do {
                // This should only happen on the first run.
                if (task != NULL) {
                    cout << "Running " << this->num << endl;
                    task->run();
                }
                guard = new boost::lock_guard<boost::mutex>(*mutex);
                task = parent->next();
                delete guard;

            } while (task != NULL);
        }
    };

    // Private memebers
    uint8_t num_threads;
    // queue holds the jobs to be run
    queue< E *, list<E *> > q;
    // Jobs move to the in_progress list when they are being executed.
    list <E *> in_progress;
    // The mutex object
    boost::mutex mutex;
    // The lock guard class provides us a simple way to lock on mutex's
    boost::lock_guard<boost::mutex> *guard;
    // Holds the threads that are processing jobs.
    boost::thread_group *thread_group;
    // Runners are responsible for processing tasks
    TaskRunner **runners;
    ev::sig *sig;

    // Get the next task from the queue
    E *next()
    {
        if (q.empty()) {
            return NULL;
        }
        E *elem = q.front();
        q.pop();
        in_progress.push_back(elem);
        return elem;
    }

    void startThreads()
    {
        guard = new boost::lock_guard<boost::mutex>(mutex);
        // Create the threads and start their event loops.
        for (uint8_t i = 0; i < num_threads; i++) {
            // Instantiate a thread and add it to the group
            thread_group->create_thread(boost::bind(&TaskRunner::process, runners[i], &mutex));
        }
        delete guard;
    }

    void stopThreads()
    {
        // Instantiate the thread here.
        if (thread_group) {
            thread_group->join_all();
        }
    }

    void startWatchers()
    {
        sig->start(SIGINT);
    }

    void stopWatchers()
    {
        sig->stop();
        ev_break(sig->loop, EVBREAK_ALL);
    }

public:
    TaskQueue()
    {
        num_threads = boost::thread::hardware_concurrency();
        if (num_threads == 0) {
            num_threads = 1;
        }
        cout << "I can create " << (int) num_threads << " threads" << endl;
        runners = new TaskRunner*[num_threads];
        for (uint8_t i = 0; i < num_threads; i++) {
            runners[i] = new TaskRunner(this, i+1);
        }
        thread_group = new boost::thread_group;
        // Set up the signal handler
        sig = new ev::sig;
        sig->set< TaskQueue<T, E>, &TaskQueue<T, E>::handle >(this);
    }

    ~TaskQueue()
    {
        for (uint8_t i = 0; i < num_threads; i++) {
            if (runners[i]) {
                delete runners[i];
                runners[i] = NULL;
            }
        }
        delete thread_group;
        delete [] runners;
        delete sig;
        thread_group = NULL;
        runners = NULL;
        sig = NULL;
    }

    // Add a task to the queue
    void add(E *task)
    {
        // We are mutating the queue so we need to lock
        guard = new boost::lock_guard<boost::mutex>(mutex);
        q.push(task);
        delete guard;
    }

    // query the queue to find remaining tasks
    uint32_t remaining() const
    {
        return q.size();
    }

    void start()
    {
        startWatchers();
        startThreads();
        // Now start the event loop.
        ev_run(sig->loop);
    }

    void stop(bool force = false)
    {
        if (!force) {
            if (!q.empty()) {
                return;
            } else if (q.empty() && in_progress.size() > 0) {
                for (typename list<E *>::iterator it = in_progress.begin(); it != in_progress.end(); ++it) {
                    E *task = (*it);
                    if (task->getStatus() == task->READY) {
                        in_progress.remove(task);
                    }
                }
            }
            if (in_progress.size() == 0) {
                stopWatchers();
            }
        } else {
            stopWatchers();
        }
    }

    void handle()
    {
        stopThreads();
        stop(true);
    }
};

// A simple typedef to be able to get a RenderTaskQueue
typedef class TaskQueue<std::string, RenderTask> RenderTaskQueue;

#endif // TASKQUEUE_H
